## Build Setup 

``` bash
# install dependencies
npm install # or yarn

# serve in dev mode
yarn dev

# build for production
yarn build
```

## License

MIT