import { api } from './index'

import MockAdapter from 'axios-mock-adapter'

const mock = new MockAdapter(api, {
	delayResponse: 500
})

mock.onPost('api/project/update').reply(function(info) {
	let data = JSON.parse(info.data)

	console.log(info.method.toLocaleUpperCase() +' - Запрос на url:'+ info.url)
	console.log('Параметры запроса:', data)


	const updateProject = data.params.project

	const project = projectsFullData.find(project => project.id === parseInt(updateProject.id))

	project.name = updateProject.name
	project.description = updateProject.description
	project.objects = updateProject.objects

	let response = {
		project: project
	}

	console.log('Ожидаемый ответ:', response)
	return [200, response]
})


mock.onPost('api/project/update/objects').reply(function(info) {
	let data = JSON.parse(info.data)

	console.log(info.method.toLocaleUpperCase() +' - Запрос на url:'+ info.url)
	console.log('Параметры запроса:', data)


	const newObject = data.params.object

	const project = projectsFullData.find(project => project.id === parseInt(data.params.id))

	const object = project.objects.find(object => object.name === newObject.name)
	object.items = newObject.items

	let response = {
		object: object
	}

	console.log('Ожидаемый ответ:', response)
	return [200, response]
})

mock.onGet('api/project').reply(function(info) {
	let data = info.params

	console.log(info.method.toLocaleUpperCase() +' - Запрос на url:'+ info.url)
	console.log('Параметры запроса:', data)

	const project = projectsFullData.find(project => project.id === parseInt(data.id))

	let response = {
		project: project
	}

	console.log('Ожидаемый ответ:', response)
	return [200, response]
})


mock.onGet('api/projects').reply(function(info) {
	let data = info.params

	console.log(info.method.toLocaleUpperCase() +' - Запрос на url:'+ info.url)
	console.log('Параметры запроса:', data)

	const projects = []

	for(const project of projectsFullData) {
		projects.push({
			id: project.id,
			name: project.name,
			description: project.description
		})
	}

	let response = {
		projects: projects
	}

	console.log('Ожидаемый ответ:', response)
	return [200, response]
})


const userList = [
	{
		selected: false,
		"id": 7,
		"email": "michael.lawson@reqres.in",
		"name": "Michael",
		"last_name": "Lawson",
		"avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/follettkyle/128.jpg"
	},
	{
		selected: true,
		"id": 8,
		"email": "lindsay.ferguson@reqres.in",
		"name": "Lindsay",
		"last_name": "Ferguson",
		"avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/araa3185/128.jpg"
	},
	{
		selected: false,
		"id": 9,
		"email": "tobias.funke@reqres.in",
		"name": "Tobias",
		"last_name": "Funke",
		"avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/vivekprvr/128.jpg"
	},
	{
		selected: true,
		"id": 10,
		"email": "byron.fields@reqres.in",
		"name": "Byron",
		"last_name": "Fields",
		"avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/russoedu/128.jpg"
	},
	{
		selected: true,
		"id": 11,
		"email": "george.edwards@reqres.in",
		"name": "George",
		"last_name": "Edwards",
		"avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/mrmoiree/128.jpg"
	},
	{
		selected: false,
		"id": 12,
		"email": "rachel.howell@reqres.in",
		"name": "Rachel",
		"last_name": "Howell",
		"avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/hebertialmeida/128.jpg"
	}
]

const suppliers = [
	{
		selected: true,
		"id": 7,
		"email": "michael.lawson@reqres.in",
		"name": "Michael",
		"last_name": "Lawson",
		"avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/follettkyle/128.jpg"
	},
	{
		selected: false,
		"id": 8,
		"email": "lindsay.ferguson@reqres.in",
		"name": "Lindsay",
		"last_name": "Ferguson",
		"avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/araa3185/128.jpg"
	},
	{
		selected: true,
		"id": 9,
		"email": "tobias.funke@reqres.in",
		"name": "Tobias",
		"last_name": "Funke",
		"avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/vivekprvr/128.jpg"
	},
	{
		selected: false,
		"id": 10,
		"email": "byron.fields@reqres.in",
		"name": "Byron",
		"last_name": "Fields",
		"avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/russoedu/128.jpg"
	},
	{
		selected: true,
		"id": 11,
		"email": "george.edwards@reqres.in",
		"name": "George",
		"last_name": "Edwards",
		"avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/mrmoiree/128.jpg"
	},
	{
		selected: false,
		"id": 12,
		"email": "rachel.howell@reqres.in",
		"name": "Rachel",
		"last_name": "Howell",
		"avatar": "https://s3.amazonaws.com/uifaces/faces/twitter/hebertialmeida/128.jpg"
	}
]

const scenarios = [
	{
		id: 1,
		selected: false,
		name: "Сценарий 1",
	},
	{
		id: 2,
		selected: false,
		name: "Сценарий 2",
	},
	{
		id: 3,
		selected: false,
		name: "Сценарий 3",
	},
	{
		id: 4,
		selected: false,
		name: "Сценарий 4",
	},
	{
		id: 5,
		selected: true,
		name: "Сценарий 5",
	},
	{
		id: 6,
		selected: false,
		name: "Сценарий 6",
	},
	{
		id: 7,
		selected: false,
		name: "Сценарий 7",
	},
	{
		id: 8,
		selected: false,
		name: "Сценарий 8",
	},
	{
		id: 9,
		selected: true,
		name: "Сценарий 9",
	},
	{
		id: 10,
		selected: false,
		name: "Сценарий 10",
	},
	{
		id: 11,
		selected: false,
		name: "Сценарий 11",
	},
	{
		id: 12,
		selected: false,
		name: "Сценарий 12",
	},
	{
		id: 13,
		selected: false,
		name: "Сценарий 13",
	},
	{
		id: 14,
		selected: false,
		name: "Сценарий 14",
	},
	{
		id: 15,
		selected: false,
		name: "Сценарий 15",
	}
]

const projectsFullData = [
	{
		id: 1,
		name: 'Первый проект',
		description: 'Новый крутой первый проект',
		objects: [
			{
				title: 'Пользователи',
				name: 'users',
				items: [
					...userList
				],
			},
			{
				title: 'Поставщики',
				name: 'suppliers',
				items: [
					...suppliers
				]
			},
			{
				title: 'Сценарии',
				name: 'scenarios',
				items: [
					...scenarios
				]
			}
		]
		
	},
	{
		id: 2,
		name: 'Второй проект',
		objects: [
			{
				title: 'Пользователи',
				name: 'users',
				items: [
					...userList
				],
			},
			{
				title: 'Поставщики',
				name: 'suppliers',
				items: [
					...suppliers
				]
			},
			{
				title: 'Сценарии',
				name: 'scenarios',
				items: [
					...scenarios
				]
			}
		]
	}
]