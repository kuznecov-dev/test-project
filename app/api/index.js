import axios from 'axios';

const api = axios.create({});

if(!process.env.production) {
	require('./mock')
}

api.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8'

export {
    api
}

export default api