import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const Projects = () => import('../pages/projects/projects.vue')
const Project = () => import('../pages/projects/project.vue')

export function createRouter () {
	return new Router({
		mode: 'history',
		fallback: false,
		routes: [
			{
				path: '/projects',
				component: Projects,
			},
			{
				path: '/projects/:id',
				component: Project
			}
		]
	})
}
