import 'regenerator-runtime/runtime'
import 'core-js/stable/promise'

import Vue from 'vue'

import { createStore } from './store'
import { createRouter } from './router'
import { sync } from 'vuex-router-sync'

import Spinner from '~/components/Spinner.vue'

export function createApp () {

	const store = createStore()
	const router = createRouter()

	sync(store, router)

	const app = new Vue({
		components: {
			Spinner
		},
		router,
		store,
	})

	return { app, router, store }
}
