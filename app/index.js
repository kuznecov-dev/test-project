import Vue from 'vue'
import { createApp } from './app'

Vue.mixin({
	beforeRouteUpdate (to, from, next) {
		const { asyncData } = this.$options
		if (asyncData) {
			asyncData({
				store: this.$store,
				route: to
			}).then(next).catch(next)
		} else {
			next()
		}
	}
})

const { app, router, store } = createApp()

if (window.__INITIAL_STATE__) {
	store.replaceState(window.__INITIAL_STATE__)
}

router.onReady(() => {
	const matchedComponents = router.getMatchedComponents()

	Promise.all(matchedComponents.map(({ asyncData }) => asyncData && asyncData({
		store,
		route: router.currentRoute
	}))).then(() => {
		app.$mount('#app')
	})

	router.beforeResolve((to, from, next) => {
		const matched = router.getMatchedComponents(to)
		const prevMatched = router.getMatchedComponents(from)
		let diffed = false

		const activated = matched.filter((c, i) => {
			return diffed || (diffed = (prevMatched[i] !== c))
		})

		const asyncDataHooks = activated.map(c => c.asyncData).filter(_ => _)
		if (!asyncDataHooks.length) {
			return next()
		}

		Promise.all(asyncDataHooks.map(hook => hook({ store, route: to })))
			.then(() => {
				next()
			})
			.catch(next)
	})

	router.beforeEach((to, from, next) => {
		store.dispatch('UPDATE_PAGE_LOAD', true)
		next()
	})

	router.afterEach((to, from) => {
		store.dispatch('UPDATE_PAGE_LOAD', false)
	})
})
