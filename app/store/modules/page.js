import * as types from '../mutation-types'

const state = {
	load: false,
	buffer: 0
}

const actions = {
	[types.UPDATE_PAGE_LOAD]: ({ commit }, status) => {
		commit('SET_PAGE_LOAD', status)
	},
}

const mutations = {
	[types.SET_PAGE_LOAD]: (state, data) => {
		if(data) {
			state.buffer += 1
		} else {
			state.buffer -= 1
		}

		if(state.buffer < 0) {
			state.buffer = 0
		}

		state.load = state.buffer
	},
}

export default {
	state,
	actions,
	mutations
}