import { api } from '~/api'
import * as types from '../mutation-types'

const state = {
	projects: [],
	project: null
}

const actions = {
	[types.GET_PROJECTS]: async ({ commit }) => {
		try {
			const response = await api.get('api/projects')

			const data = response.data

			if(data.projects && data.projects.length) {
				commit('SET_PROJECTS', data.projects)
			}
		} catch (error) {
			console.log('error', error)
		}
	},
	[types.GET_PROJECT]: async ({ commit }, params) => {
		try {
			const response = await api.get('api/project', {
				params
			})

			const data = response.data

			if(data.project) {
				commit('SET_PROJECT', data.project)
			}
		} catch (error) {
			console.log('error', error)
		}
	},
	[types.UPDATE_PROJECT_OBJECT]: async ({ commit, dispatch }, params) => {
		dispatch('UPDATE_PAGE_LOAD', true)

		try {
			const response = await api.post('api/project/update/objects', {
				params
			})

			const data = response.data

			if(data.object) {
				commit('SET_UPDATE_PROJECT_OBJECT', data.object)
			}
		} catch (error) {
			console.log('error', error)
		}

		dispatch('UPDATE_PAGE_LOAD', false)
	},
	[types.UPDATE_PROJECT]: async ({ commit, dispatch }, params) => {
		dispatch('UPDATE_PAGE_LOAD', true)

		try {
			const response = await api.post('api/project/update', {
				params
			})

			const data = response.data

			if(data.project) {
				commit('SET_PROJECT', data.project)
			}
		} catch (error) {
			console.log('error', error)
		}

		dispatch('UPDATE_PAGE_LOAD', false)
	}
}

const mutations = {
	[types.SET_PROJECTS]: (state, data) => {
		state.projects.splice(0, state.projects.length)
		state.projects.push(...data)
	},
	[types.SET_PROJECT]: (state, data) => {
		state.project = data
	},
	[types.SET_UPDATE_PROJECT_OBJECT]: (state, data) => {
		const object = state.project.objects.find(object => object.name === data.name)
		object.items.splice(0, object.items.length)
		object.items.push(...data.items)
	}
}

export default {
	state,
	actions,
	mutations
}