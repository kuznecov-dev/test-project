import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import page from './modules/page'
import data from './modules/data'

export function createStore () {
	return new Vuex.Store({
		modules: {
			page,
			data
		}
	})
}
