const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

const TerserPlugin = require('terser-webpack-plugin');

const isProd = process.env.NODE_ENV === 'production';

const minimizer = isProd ? [
	new TerserPlugin()
] : [];


module.exports = {
    mode: isProd ? 'production' : 'development',
    entry: './app/index.js',
    devtool: isProd ? false : 'inline-cheap-module-source-map',
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    compilerOptions: {
                        preserveWhitespace: false
                    }
                }
            },
	        {
		        test: /\.scss$/,
		        use: ['vue-style-loader', 'css-loader', 'sass-loader', {
			        loader: 'style-resources-loader',
			        options: {
				        patterns: [
					        './app/assets/styles/index.scss',
				        ]
			        }
		        }]
	        }
        ]
    },
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js',
	        '~': path.resolve(__dirname, './app'),
        }
    },
    output: {
        publicPath: '/dist/js/',
        path: __dirname + '/dist/js',
        filename: 'bundle.js',
    },
    optimization: {
	    minimize: isProd,
        minimizer: minimizer
    },
    plugins: [
        new VueLoaderPlugin()
    ]
};